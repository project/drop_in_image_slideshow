CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainersss


INTRODUCTION
------------

This Drop In Image Slideshow Drupal module is your regular 
image slideshow module, except each image is dropped into view. 
this effect that works in all major browsers. The slideshow stops 
dropping when the mouse is over it. In the admin we have option 
to enter image location, width and height of the slideshow.


FEATURES:
---------

1. Easy to customize.
2. Support all browser.
3. Automatically pauses on mouse over.
4. Random order option available.


REQUIREMENTS
------------

Just Drupal module Drop In Image Slideshow moudule files


INSTALLATION
------------

1. Install the module as normal, see link for instructions.
Link: https://www.drupal.org/documentation/install/
modules-themes/modules-8


CONFIGURATION
-------------

1. Go to Admin >> Extend page and check the Enabled 
check box near to the module Drop In Image Slideshow 
and then click the Install button at the bottom.

2. Go to Admin >> Structure >> Block layout page, 
there you can see button called Place Block near 
each available blocks.

3. Go to Admin >> Structure >> Blocks layout page and 
click Place Block button to add Block. If you want to 
add Block in your first sidebar, click Place Block button 
near your Sidebar first title. It will open Place block 
window. In that window again click Place Block button near 
Drop In Image Slideshow.

4. Now open your website front end and see the Drop In Image 
Slideshow module in the selected location. At first it use 
the default image, go to configuration link of the module 
and update the default module setting. see the below screen.


MAINTAINERS
-----------

Current maintainers:

 * Gopi RAMASAMY 
https://www.drupal.org/user/1388160
http://www.gopiplus.com/work/about/
http://www.gopiplus.com/extensions/2012/
03/drop-in-image-slideshow-gallery-drupal-module/
 
Requires - Drupal 8
License - GPL (see LICENSE)
