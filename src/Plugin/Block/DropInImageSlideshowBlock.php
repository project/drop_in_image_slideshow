<?php

namespace Drupal\drop_in_image_slideshow\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'DropInImageSlideshow' Block.
 *
 * @Block(
 * id = "drop_in_image_slideshow",
 * admin_label = @Translation("Drop In Image Slideshow"),
 * )
 */
class DropInImageSlideshowBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['drop_images'])) {
      $drop_images = $config['drop_images'];
    }
    else {
      $drop_images = "";
    }

    if (!empty($config['drop_width'])) {
      $drop_width = $config['drop_width'];
    }
    else {
      $drop_width = 250;
    }

    if (!empty($config['drop_height'])) {
      $drop_height = $config['drop_height'];
    }
    else {
      $drop_height = 170;
    }

    if (!empty($config['drop_pause'])) {
      $drop_pause = $config['drop_pause'];
    }
    else {
      $drop_pause = 3000;
    }

    // 5 minutes, 5 * 60 = 300.
    $output[]['#cache']['max-age'] = 300;
    $values = [
      'drop_images' => $drop_images,
      'drop_width' => $drop_width,
      'drop_height' => $drop_height,
      'drop_pause' => $drop_pause,
    ];

    $markup = $this->dropInBlock($values);
    $output[] = [
      '#markup' => Markup::create($markup),
      '#allowed_tags' => ['script', 'div', 'a', 'span'],
    ];
    $output['#attached']['library'][] = 'drop_in_image_slideshow/drop_in_image_slideshow';
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  private function dropInBlock(array $values) {
    $drop_images = $values['drop_images'];
    $drop_width = $values['drop_width'];
    $drop_height = $values['drop_height'];
    $drop_pause = $values['drop_pause'];

    if ($drop_images == "") {
      $drop_block_dtl = "Image details are not available.";
      return $drop_block_dtl;
    }

    if (!is_numeric($drop_width)) {
      $drop_width = 250;
    }

    if (!is_numeric($drop_height)) {
      $drop_height = 170;
    }

    if (!is_numeric($drop_pause)) {
      $drop_pause = 3000;
    }

    // Remove any extra \r characters left behind.
    $drop_image_link_Ar = explode("\n", $drop_images);
    $drop_image_link_Ar = array_filter($drop_image_link_Ar, 'trim');

    $wpis_returnstr = "";
    $wpis_count = 0;
    $drop_image = "";
    $drop_link = "#";
    foreach ($drop_image_link_Ar as $drop_image_link) {
      $drop_image_link = preg_replace("/\r|\n/", "", $drop_image_link);

      if (strpos($drop_image_link, ']-[') !== FALSE) {
        $drop_image_link = explode("]-[", $drop_image_link);
        $drop_image = $drop_image_link[0];
        $drop_link = $drop_image_link[1];
      }
      elseif (strpos($drop_image_link, '] - [') !== FALSE) {
        $drop_image_link = explode("] - [", $drop_image_link);
        $drop_image = $drop_image_link[0];
        $drop_link = $drop_image_link[1];
      }
      elseif (strpos($drop_image_link, ']- [') !== FALSE) {
        $drop_image_link = explode("]- [", $drop_image_link);
        $drop_image = $drop_image_link[0];
        $drop_link = $drop_image_link[1];
      }
      elseif (strpos($drop_image_link, '] -[') !== FALSE) {
        $drop_image_link = explode("] -[", $drop_image_link);
        $drop_image = $drop_image_link[0];
        $drop_link = $drop_image_link[1];
      }
      else {
        if ($drop_image_link <> "") {
          $drop_image = $drop_image_link;
          $drop_link = "#";
        }
      }

      $drop_image = ltrim($drop_image, '[');
      $drop_image = rtrim($drop_image, ']');

      $drop_link = ltrim($drop_link, '[');
      $drop_link = rtrim($drop_link, ']');

      $wpis_str = '["' . $drop_image . '", "' . $drop_link . '", ""]';
      $wpis_returnstr = $wpis_returnstr . "wpis_images[$wpis_count]=$wpis_str; ";
      $wpis_count++;
    }

    $drop_block_dtl = "";
    $drop_block_dtl = $drop_block_dtl . '<script type="text/javascript"> ';
    $drop_block_dtl = $drop_block_dtl . 'var wpis_images=new Array(); ';
    $drop_block_dtl = $drop_block_dtl . $wpis_returnstr;
    $drop_block_dtl = $drop_block_dtl . 'new wpis(wpis_images, ' . $drop_width . ', ' . $drop_height . ', ' . $drop_pause . ') ';
    $drop_block_dtl = $drop_block_dtl . '</script> ';

    return $drop_block_dtl;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['drop_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter width'),
      '#description' => $this->t('Enter slideshow width, only number. (Example: 250)'),
      '#default_value' => isset($config['drop_width']) ? $config['drop_width'] : '250',
    ];

    $form['drop_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter height'),
      '#description' => $this->t('Enter slideshow height, only number. (Example: 170)'),
      '#default_value' => isset($config['drop_height']) ? $config['drop_height'] : '170',
    ];

    $form['drop_pause'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter pause duration'),
      '#description' => $this->t('Pause between image change (millisec), only number. (Example: 3000)'),
      '#default_value' => isset($config['drop_pause']) ? $config['drop_pause'] : '3000',
    ];

    $form['drop_images'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter image details'),
      '#description' => $this->t('Please enter image url. One image url per line. [Where is the picture located on the internet]-[When someone clicks on the picture, where do you want to send them] <br /> Example : [http://www.gopiplus.com/work/wp-content/uploads/pluginimages/250x167/250x167_1.jpg]-[http://www.gopiplus.com/]'),
      '#default_value' => isset($config['drop_images']) ? $config['drop_images'] : '[http://www.gopiplus.com/work/wp-content/uploads/pluginimages/250x167/250x167_1.jpg]-[http://www.gopiplus.com/]',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['drop_width'] = $form_state->getValue('drop_width');
    $this->configuration['drop_height'] = $form_state->getValue('drop_height');
    $this->configuration['drop_pause'] = $form_state->getValue('drop_pause');
    $this->configuration['drop_images'] = $form_state->getValue('drop_images');
  }

}
